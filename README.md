# PhotoLine Assets and Presets Library

## What is this all about?

This is a presets library for [PhotoLine](https://www.pl32.com/). PhotoLine is a powerful yet lightweight all-purpose image and graphics design software with support for strong bitmap editing, yet also features good vector editing tools.

PhotoLine does not include many presets. This project aims to fill that void. 

**important** This asset library is made for the latest beta 24.40b10 and later. This version introduces the option to group colours, patterns, brushes, textures, etc. It may not work correctly on previous releases.

The latest beta is available in the *Betatester unter sich* thread on the [PhotoLine forums](https://www.pl32.com/forum3/index.php).

## Asset & Preset Categories

The following asset categories are in the works or planned:

- [ ] An extensive set of colour palettes (first version released)
- [ ] An extensive set of default gradients (first version released)
- [ ] An extensive set of default textures (in progress)
- [ ] An extensive set of default patterns
- [ ] An extensive set of default assets for comics (balloons, effects, etc.) (in progress)
- [ ] Actions: CG Post Processing Pack (in progress)

- [ ] Standard document templates for print and comics
- [ ] Standard document templates for screen design (games, mobile, presentations, etc)
- [ ] Standard video and animation document templates
- [ ] Standard texture format document templates
- [ ] a set of comic design and layout related assets (text balloons, effects, document templates)

- [ ] A broad set of assets (cliparts, masks, frames, and other asset types)
- [ ] Crop presets
- [ ] Specialist document templates (lighting masking, index painting, and more)
- [ ] Gamut mask tool(s) (in progress)

- [ ] and more... ?


## Installation

Color scheme file: open the colour palettes panel ('colors') and click the panel menu button. Merge.

If the *load* option is selected existing colour palettes will be replaced.

This colour library pack maintains the default PhotoLine colour swatches: if no custom colours are present, loading the colour library will put the default colours in its own group at the top.

![](img/colour_import.png)

Gradients: same process, but use the Gradients panel instead. Gradients are still in progress.

Textures: same process, but use the Textures panel instead. Not much here yet.

## Contributing
Contributions are welcome!

## Authors and acknowledgment
Where possible references and links are given to the respective authors.
- [ ] Krita colour palette(s): Krita.org
- [ ] Retro colour palette(s): partly sourced from lospec.com
- [ ] Pixel art colour palette(s): mostly sourced from lospec.com
- [ ] github.com/Autocrit/Pantone-color-libraries
- [ ] https://yeun.github.io/open-color/
- [ ] https://infoc383.myportfolio.com/hi-fi-color-for-comics
- [ ] https://github.com/mhulse/swatches
- [ ] https://www.dunnedwards.com/colors/download-photoshop-swatches/
- [ ] https://krita-artists.org/t/358-copic-colour-swatches/41207
- [ ] https://www.fabiocrameri.ch/colourmaps/
- [ ] http://www.geoscienceatomprobe.org/downloads.html



***
***
## Updated Icon Set & Photoshop Shortcuts

An updated clean and modern looking icon set that replaces PhotoLine's default icons is available. User *Shijan* is the maintainer of that project. 
Follow the discussion on the [PhotoLine forum](https://www.pl32.com/forum3/viewtopic.php?t=6302).
The latest version of the icon set is available on shijan's [DevianArt Gallery](https://www.deviantart.com/shijan/art/825902404).

![Icon Set Screenshot!](https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/7d4eb80e-752e-4ac0-939d-ce6767de1d5f/ddnpydg-ec508233-2e65-4c0f-8527-e62c7b58a775.jpg/v1/fit/w_786,h_513,q_70,strp/photoline_ui_icons_customization_project_by_shijan_ddnpydg-414w-2x.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9NTEzIiwicGF0aCI6IlwvZlwvN2Q0ZWI4MGUtNzUyZS00YWMwLTkzOWQtY2U2NzY3ZGUxZDVmXC9kZG5weWRnLWVjNTA4MjMzLTJlNjUtNGMwZi04NTI3LWU2MmM3YjU4YTc3NS5qcGciLCJ3aWR0aCI6Ijw9Nzg2In1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmltYWdlLm9wZXJhdGlvbnMiXX0.wJNqAOtwb-GM1vrHOthGzMCRhotYSporq_q7dnl_4jc)


### Photoshop Shortcuts
Shijan also created a shortcuts preference file that emulates Photoshop's shortcuts. To install these, follow the instructions on shijan's [DevianArt Gallery](https://www.deviantart.com/shijan/art/825902404). These are part of the same icon set package.
